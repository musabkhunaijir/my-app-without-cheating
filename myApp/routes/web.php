<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// use App\my_user;
Route::get('/', function () {
    // return view('welcome', [
    //     'name' => 'world'
    // ]);
        // $tasks = [
        //     'read laravel',
        //     'watch inso tut'

        // ];

    $users = DB::table('users')->get();
    // $users = App/my_user::all();
    // return $tasks;
    return view('welcome', compact('users'));
    // return view('welcome')-> with('name','musab');
});

Route::get('/users/{id}', function ($id) {
    // dd($id);
    $users = DB::table('users')->where('id','=',$id)->get();
    // $users = App/my_user::find($id);
    return view('welcome', compact('users'));
});

Route::get('/users', function(){
    $users = DB::table('users')->get();
    // $users = App/MyUser::all();
    return view('welcome', compact('users') );
} );